//
//  StrongAuth.swift
//  CAReactNative
//
//  Created by ThunderFlash on 12/12/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import UIKit

@objc(StrongAuth)
class StrongAuth: RCTEventEmitter {
  
  struct Key {
    static let token = "token"
  }
  
  static let shared = StrongAuth()
  
  let authenticationHandler = AuthenticationHandler()
  
  override init() {
    super.init()
    
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(self.getFirebaseCloudMessagingToken(notification:)),
                                           name: Notification.Name("FCMToken"), object: nil)
  }
  
  @objc
  func getTransaction(_ transactionId: String,
                      accountId: String,
                      accountType: String,
                      callback: @escaping RCTResponseSenderBlock) {
    
    guard let transactionDetailsResponse = authenticationHandler.getTransaction(transactionId, accountId: accountId, accountType: accountType) else { return }
    
    let response = ["status": transactionDetailsResponse.status,
                    "errorCode": transactionDetailsResponse.errorCode]
    
    callback([NSNull(), response])
  }
  
  @objc
  func setPin(_ accountId: String,
              pin: String,
              resolver resolve: RCTPromiseResolveBlock,
              rejecter reject:RCTPromiseRejectBlock) -> Void {
    //"11112"
    guard let activationAcknowledgementResponse1 = authenticationHandler.setPin(pin),
      let pinResponse = activationAcknowledgementResponse1.firstObject as? ActivationAcknowledgementResponse else { return }
    
    let responseArray = [pinResponse.status,
                         pinResponse.accountId,
                         pinResponse.errorCode,
                         pinResponse.displayName]
    
    resolve(responseArray)
  }
  
  @objc
  func verifySingleAccountActivation(_ activationCode: String,
                                     activationRefId: String,
                                     postURL: String,
                                     displayName: String,
                                     pin: String,
                                     resolver resolve: RCTPromiseResolveBlock,
                                     rejecter reject:RCTPromiseRejectBlock) -> Void {
    
    let multipleAccountActivationParam = MultipleAccountActivationParams(data: activationCode, activationRefId, displayName, "")
    let multipleAccounts: NSMutableArray = [multipleAccountActivationParam!]
    
    guard let multipleAccountActivationDetails = authenticationHandler.verifyActivation(multipleAccounts, postURL),
      let multipleAccountActivationStatus = multipleAccountActivationDetails.multipleAccountActivationStatusArray.firstObject as? MultipleAccountActivationStatus else { return }
    
    guard let status = multipleAccountActivationDetails.status,
      let accountId = multipleAccountActivationStatus.accountId else { return }
    
    let errorCode = multipleAccountActivationDetails.errorCode == nil ? "SUCCESS" : multipleAccountActivationDetails.errorCode
    let isPINRequired = multipleAccountActivationDetails.isPINRequired == true ? "true" : "false"
    let activationResponse = [status,
                              errorCode,
                              accountId,
                              isPINRequired]
    
    resolve(activationResponse)
  }
  
  @objc
  func verifyActivation(_ multipleAccountActivationParams: [Dictionary<String, String>],
                        callback: @escaping RCTResponseSenderBlock) {
    
    var multipleAccountActivationParamsArray = [MultipleAccountActivationParams]()
    var serverURL = ""
    
    multipleAccountActivationParams.forEach { account in
      guard let activationCode = account["activationCode"],
        let activationRefId = account["activationRefId"],
        let postURL = account["postURL"],
        let displayName = account["displayName"],
        let pin = account["pin"],
        let multipleAccountActivationParam = MultipleAccountActivationParams(data: activationCode, activationRefId, displayName, pin)
        else { return }
      
      serverURL = postURL
      
      multipleAccountActivationParamsArray.append(multipleAccountActivationParam)
    }
    
    let multipleAccountActivationParamsMutableArray = NSMutableArray(array: multipleAccountActivationParamsArray)
    
    DispatchQueue.main.async {
      guard let multipleAccountActivationDetails = self.authenticationHandler.verifyActivation(multipleAccountActivationParamsMutableArray, serverURL),
        let multipleAccountActivationStatus = multipleAccountActivationDetails.multipleAccountActivationStatusArray.firstObject as? MultipleAccountActivationStatus else { return }
      
      let response = ["status": multipleAccountActivationStatus.status,
                      "errorCode": multipleAccountActivationStatus.errorCode,
                      "accountId": multipleAccountActivationStatus.accountId,
                      "referenceId": multipleAccountActivationStatus.referenceId,
                      "displayName": multipleAccountActivationStatus.displayName]
      
      callback([NSNull(), response])
    }
  }
    
  @objc
  func changePIN(_ oldPin: String,
                 newPin: String,
                 callback: @escaping RCTResponseSenderBlock) {
    
    guard let changePINResponse = authenticationHandler.changePIN(oldPin, newPin) else { return }
    
    let response = ["status": changePINResponse.status,
                    "errorCode": changePINResponse.errorCode]
    
    callback([NSNull(), response])
  }
  
  @objc
  func verifyPIN(_ pin: String,
                 autoEnrollId: String,
                 callback: @escaping RCTResponseSenderBlock) {
    
    guard let verifyPINResponse = authenticationHandler.verifyPIN(pin, autoEnrollId) else { return }
    
    let response = ["status": verifyPINResponse.status,
                    "errorCode": verifyPINResponse.errorCode]
    
    callback([NSNull(), response])
  }
  
  @objc
  func isPINSetup(callback: @escaping RCTResponseSenderBlock) {
    
    callback([NSNull(), authenticationHandler.isPINSetup()])
  }
}

extension StrongAuth {
  
  @objc
  func getFirebaseCloudMessagingToken(notification: NSNotification) {
    let defaults = UserDefaults.standard
    
    guard let userInfo = notification.userInfo else { return }
    
    if let fcmToken = userInfo[Key.token] as? String {
      defaults.set(fcmToken, forKey: Key.token)
      defaults.synchronize()
    }
  }
  
  @objc
  override static func requiresMainQueueSetup() -> Bool {
    return false
  }
  
  @objc
  override func constantsToExport() -> [AnyHashable : Any]! {
    return ["initialCount": 434]
  }
  
  override func supportedEvents() -> [String]! {
    return []
  }
}

//
//  UpdateAuthenticationResponse.h
//  M3DS
//
//  Created by Babu Singh  on 7/19/18.
//  Copyright © 2018 Deepak Prasad. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * The UpdateAuthenticationResponse class contains information about the results of the Update Authentication Type operation.
 * An object of this class is sent in the response of the updateAuthenticationType method.
 */
@interface UpdateAuthenticationResponse : NSObject


/**
 * This property determines whether the Update Authentication Type operation is SUCCESS/FAILURE
 */
@property(retain, nonatomic) NSString * status;

/**
 * This property provides the errorCode that gets returned depending on the scenario wherein
 * the UpdateAuthenticationResponse object gets populated.
 */
@property(retain, nonatomic) NSString * error_code;


/**
 *  Method                  - initWithData
 *  Description             - This API initializes the <CODE>UpdateAuthenticationResponse</CODE> object
 *                            with the status,errorCode
 *  Args                    - NSString aStatus,NSString error_code,
 *  Return Type             - This API returns the type UpdateAuthenticationResponse object
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
-(id)initWithData :(NSString *)status
                  :(NSString *)error_code;

/**
 *  Method                  - initWithStatus
 *  Description             - This API initializes the <CODE>UpdateAuthenticationResponse</CODE> object
 *                            with the Status
 *  Args                    - NSString aStatus
 *  Return Type             - This API returns the type UpdateAuthenticationResponse object
 *                            UpdateAuthenticationResponse Parameters -
 *
 *                                  status : SUCCESS
 *
 *                            The status can be the following value:
 *                            •    SUCCESS: Operation was successful.
 *
 *
 **/
-(id)initWithStatus:(NSString *)status;

@end

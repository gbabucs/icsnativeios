//
//  MultipleAccountActivationStatus.h
//  M3DS
//
//  Created by husna02 on 14/11/18.
//  Copyright © 2018 Deepak Prasad. All rights reserved.
//

#ifndef MultipleAccountActivationStatus_h
#define MultipleAccountActivationStatus_h


#endif /* MultipleAccountActivationResponse_h */


/**
 * The MultipleAccountActivationResponse class contains account activation details.
 * An object of this class is sent in the response of the verifyActivation method.
 */
@interface MultipleAccountActivationStatus: NSObject


/**
 private String status;
 private String displayName;
 private String accountId;
 private String errorCode;
 private String referenceId;*/

@property(nonatomic, strong) NSString * status;
@property(nonatomic, strong) NSString * displayName;
@property(nonatomic, strong) NSString * accountId;
@property(nonatomic, strong) NSString * errorCode;
@property(nonatomic, strong) NSString * referenceId;


- (id)initWithData :(NSString *)status :(NSString *)accountId :(NSString *)referenceId  :(NSString *)displayName :(NSString *)errorCode;







@end

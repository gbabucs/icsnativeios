//
//  Modality.h
//  M3DS
//
//  Created by husna02 on 05/06/18.
//  Copyright © 2018 Deepak Prasad. All rights reserved.
//
#import "AuthenticationType.h"
#import "AuthenticationTypeStatus.h"

/**
 *  This Authentication class contains the AuthenticationType and AuthenticationTypeStatus enums.
 *  This class manages the various authentications types, such as USER_PIN and BIOMETRIC.
 */
@interface Authentication : NSObject


/**
 * This property represents the type of Authentication supported and configured on a User's Device
 * at any point of time.
 * Valid Authentication Types - User PIN, Biometric Auth, Device PIN Auth.
 */
@property AuthenticationType  authenticationType;

/**
 * This property represents the status of the Authentication Type supported and configured on a User's Device
 * at any point of time.
 * Valid Authentication Type Status - Enabled, Disabled, Not Applicable.
 */
@property AuthenticationTypeStatus   authenticationTypeStatus;


/**
 *  Method                  - initWithData
 *  Description             - This API initializes the <CODE>Authentication</CODE> object
 *                            with the authenticationType, authenticationTypeStatus parameters
 *  Args                    - AuthenticationType authenticationType,AuthenticationTypeStatus authenticationTypeStatus
 *  Return Type             - This API returns the type Authentication object
 *                            Authentication  -
 *                              AuthenticationType        : Type of Authentication being supported
 *                              AuthenticationTypeStatus  : Status of the Authentication Type.
 *
 **/
-(id) initWithData :(AuthenticationType) mAuthenticationType :  (AuthenticationTypeStatus) mAuthenticationTypeStatus;

@end





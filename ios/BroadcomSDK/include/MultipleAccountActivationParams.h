//
//  AccountActivationRequest.h
//  M3DS
//
//  Created by husna02 on 12/11/18.
//  Copyright © 2018 Deepak Prasad. All rights reserved.
//

#ifndef MultipleAccountActivationParams_h
#define MultipleAccountActivationParams_h


#endif /* AccountActivationParams_h */

/**
 * The AccountActivationParams class contains account activation request details.
 * An object of this class is sent to the verifyActivation method.
 */
@interface MultipleAccountActivationParams : NSObject

/**
 * This property represents the activationCode that would be provided by the User to be validated by the
 * CA Strong Authentication Service to provision the user account on their device.
 *
 */
@property(nonatomic, strong) NSString * activationCode;

/**
 * This property represents the reference ID that is mapped to the activation code
 *
 */
@property(nonatomic, strong) NSString * activationRefId;

/**
 * This property represents the displayName that is provided in the request
 */
@property(nonatomic, strong) NSString * displayName;



/**
 * This property represents whether PIN Setup is done on the device or not.
 */
@property(nonatomic, strong) NSString * pin;


/**
*  Method                  - initWithData
*  Description             - This API initializes the <CODE>AccountActivationRequest</CODE> object
*                            with the activationCode, activationRefId,
*  Args                    - NSString activationCode, NSString activationRefId, NSString displayName, Boolean isPINSetupDone
*  Return Type             - This API returns the type AccountActivationRequest object
*
**/
- (id)initWithData :(NSString *)activationCode :(NSString *)activationRefId :(NSString *)displayName :(NSString *)pin;


-(NSMutableDictionary *)createMultipleAccountActivationParamsDict:(NSMutableArray <MultipleAccountActivationParams *>*) multipleAccountActivationParamsArray:(BOOL)isMasterAccountPresent:(NSDictionary *)deviceParamDict;

-(BOOL)areValidActivationParams:(NSMutableArray <MultipleAccountActivationParams *>*) activationParamsArray;



@end

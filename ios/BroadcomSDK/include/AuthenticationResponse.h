//
//  AuthenticationDetails.h
//  ecomm
//
//  Created by Deepak Prasad on 3/19/15.
//  Copyright (c) 2015 CA Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransactionDetailsResponse.h"
#import "Header.h"

/**
 * The AuthenticationResponse class contains information about user authentication results.
 * An object of this class is sent in the response of the
 * authenticateUsingConsent and authenticateUsingUserPin methods.
 */
@interface AuthenticationResponse : NSObject

/**
 * This property uniquely represents the transaction done by user.It is used to identify the transaction both by
 *  CA Strong Authentication Server and SDK.
 */
@property(retain, nonatomic) NSString * transactionId;

/**
 * This property determines whether the status is SUCCESS/FAILURE in the
 * AuthenticationResponse object
 */
@property(retain, nonatomic) NSString * status;

/**
 * This property determines whether User is allowed to re-attempt authentication a given transaction in case of failure
 * This property is set by the CA Strong Authentication Server and sent to the SDK.
 */
@property(nonatomic) BOOL attemptAgain;

/**
 * This property provides the errorCode that gets returned depending on the scenario wherein
 * the AuthenticationResponse object gets populated.
 */
@property(retain, nonatomic) NSString * errorCode;

/**
 *  Method                  - initWithData
 *  Description             - This API initializes the <CODE>AuthenticationResponse</CODE> object
 *                            with the Status,DisplayName
 *  Args                    - NSString aTransactionId, NSString aStatus, BOOL attempAgain
 *  Return Type             - This API returns the type AuthenticationResponse object
 *                            AuthenticationResponse  -
 *                              status : SUCCESS/FAILURE
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
- (id)initWithData: (NSString *)aTransactionId Status:(NSString *)aStatus AttemptAgain:(BOOL)aAttemptAgain;

/**
 *  Method                  - initWithStatus
 *  Description             - This API initializes the <CODE>AuthenticationResponse</CODE> object with the Status
 *  Args                    - NSString status
 *  Return Type             - This API returns the type AuthenticationResponse object
 *                            AuthenticationResponse  Parameters -
 *                              status : SUCCESS/FAILURE
 *
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation was successful.
 *
 **/
-(id)initWithStatus:(NSString *)aStatus;


/**
 *  Method                  - initWithErrorCode
 *  Description             - This API initializes the <CODE>AuthenticationResponse</CODE> object with the errorCode
 *  Args                    - NSString status
 *  Return Type             - This API returns the type AuthenticationResponse object
 *                            AuthenticationResponse
 *
 *
 *                            The status can be only be FAILURE:
 *                            •    FAILURE: Operation failed.
 *                                  If the status is FAILURE, then one of the below error codes will be returned in the response.
 *                                  -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                  -    ERROR_SERVER_UNREACHABLE: Server is unreachable.
 *                                  -    ERROR_ACCOUNT_NOT_FOUND: Invalid account.
 *                                  -    ERROR_SERVER_INTERNAL: Server has returned an error.
 *                                  -    ERROR_INVALID_AUTHENTICATION_TYPE: Invalid AuthenticationType object present in the request.
 *
 */
-(id) initWithErrorCode :(NSString *)errorCode;

@end

//
//  VerifyPINResponse.h
//  M3DS
//
//  Created by husna02 on 02/07/18.
//  Copyright © 2018 Deepak Prasad. All rights reserved.
//

#ifndef VerifyPINResponse_h
#define VerifyPINResponse_h
#import "Header.h"

#endif /* VerifyPINResponse_h */

/**
 * The VerifyPinResponse class contains information about the results of the Verify PIN operation.
 * An object of this class is sent in the response of the verifyPin method.
 */
@interface VerifyPINResponse : NSObject


/**
 * This property represents the transaction id for the transaction in progress
 */
@property(retain, nonatomic) NSString * transactionId;

/**
 * This property determines whether User is allowed to re-attempt authentication a given transaction in case of failure
 * This property is set by the CA Strong Authentication Server and sent to the SDK.
 */
@property(nonatomic) bool attemptAgain;

/**
 * This property provides the autoEnrollId that gets returned depending on the scenario wherein
 * the VerifyPINResponse object gets populated.
 */
@property(retain, nonatomic) NSString * autoEnrollId;

/**
 * This property determines whether the Verify PIN operation is SUCCESS/FAILURE
 */
@property(retain, nonatomic) NSString * status;



/**
 * This property provides the errorCode that gets returned depending on the scenario wherein
 * the VerifyPINResponse object gets populated.
 */
@property(retain, nonatomic) NSString * errorCode;



/**
 *  Method                  - initWithData
 *  Description             - This API initializes the <CODE>VerifyPINResponse</CODE> object
 *                            with the status,errorCode
 *  Args                    - NSString aStatus,NSString errorCode,
 *  Return Type             - This API returns the type VerifyPINResponse object
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
-(id)initWithData :(NSString *)status
                  :(NSString *)errorCode;

-(id)initWithAttempts :(NSString *)status
                  :(bool)attemptAgain
                  :(NSString *)errorCode;

-(id)initWithData:(NSString *) transactionId :(NSString *)aStatus :(NSString *)errorCode;

-(id)initWithAttempts:(NSString *) transactionId :(NSString *)autoEnrollId:(NSString *)aStatus:(NSString *)errorCode:(bool)attemptAgain ;

/**
 *  Method                  - initWithStatus
 *  Description             - This API initializes the <CODE>VerifyPINResponse</CODE> object
 *                            with the Status
 *  Args                    - NSString aStatus
 *  Return Type             - This API returns the type VerifyPINResponse object
 *                            VerifyPINResponse Parameters -
 *
 *                                  status : SUCCESS
 *
 *                            The status can be the following value:
 *                            •    SUCCESS: Operation was successful.
 *
 *
 **/
-(id)initWithStatus:(NSString *)status;


/**
 *  Method                  - initWithErrorCode
 *  Description             - This API initializes the <CODE>VerifyPINResponse</CODE> object with the errorCode
 *  Args                    - NSString errorCode
 *  Return Type             - This API returns the type VerifyPINResponse object
 *                            VerifyPINResponse
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If status is FAILURE,
 *                                   then one of the below error codes will be returned in the response object.
 *                                    -    ERROR_INVALID_PIN: Invalid user PIN.
 *                                    -    ERROR_NO_ACTIVE_ACCOUNTS: No active accounts are present.
 *                                    -    ERROR_SERVER_INTERNAL: Server has returned an error.
 *
 *
 * Return Type              - <CODE> VerifyPINResponse </CODE>
 **/
-(id)initWithErrorCode: (NSString *)errorCode;

@end



//
//  ProfileInfo.h
//  M3DS
//
//  Created by husna02 on 26/07/18.
//  Copyright © 2018 Deepak Prasad. All rights reserved.
//

#ifndef ProfileInfo_h
#define ProfileInfo_h


#endif /* ProfileInfo_h */
#import "Header.h"

/**
 *   The ProfileInfo class contains information about the Device details like - Device Nick-name, Device ID.
 *   An object of this class is sent in the response of the getProfileInfo method.
 */
@interface ProfileInfo : NSObject

/**
 * This property represents the device Nick Name that would be provided by the User while bootstrapping the
 * App which uses CA Strong Authentication SDK. This device Nick-name would be used to identify this User's
 * device in conjunction with the deviceID on CA Strong Authentication as Service.
 */
@property(retain, nonatomic) NSString * deviceNickName;

/**
 * This property represents the unique ID generated and given to the User's Device by the
 * CA Strong Authentication Service. This will uniquely identify the User's device in conjunction with the
 * Device Nick-name on the CA Strong Authentication as Service side.
 */
@property(retain, nonatomic) NSString * deviceId;

/**
 * This property determines whether retrieving ProfileInfo is SUCCESS/FAILURE
 */
@property(retain, nonatomic) NSString * status;

/**
 * This property provides the errorCode that gets returned depending on the scenario wherein
 * the ProfileInfo object gets populated.
 */
@property(retain, nonatomic) NSString * errorCode;


/**
 *  Method                  - initProfileInfo
 *  Description             - This API initializes the <CODE>ProfileInfo</CODE> object
 *                            with the deviceNickName,deviceId
 *  Args                    - NSString deviceNickName,NSString deviceId,
 *  Return Type             - This API returns the type ProfileInfo object
 *
 **/
-(id)initProfileInfo :(NSString *)deviceNickName
                  :(NSString *)deviceId;


/**
 *  Method                  - initWithErrorCode
 *  Description             - This API initializes the <CODE>ProfileInfo</CODE> object with the errorCode
 *  Args                    - NSString errorCode
 *  Return Type             - ProfileInfo  - (NSString *)deviceNickName - The Name provided by the User
 *                                                                        during installation of the App
 *                                         - (NSString *)deviceId       - SAaaS generated User Device ID
 *                            In ProfileInfo, The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If status is FAILURE,
 *                                   then one of the below error codes will be returned in the response object.
 *                                   -   PROFILE_INFO_NOT_AVAILABLE: No profile available in the device.
 *
 * Return Type              - <CODE> ProfileInfo </CODE>
 **/
-(id)initWithErrorCode :(NSString *)_errorCode;


@end

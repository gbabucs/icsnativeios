//
//  TransactionDetails.h
//  ecomm
//
//  Created by Deepak Prasad on 3/19/15.
//  Copyright (c) 2015 CA Technologies. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "AuthenticationType.h"
#import "Header.h"

/**
 *  The TransactionDetailsResponse class contains information about pending transactions.
 **/
@interface TransactionDetailsResponse : NSObject

/**
 * This property determines whether the status is SUCCESS/FAILURE in the
 * TransactionDetailsResponse object
 */
@property(retain, nonatomic) NSString * status;

/**
 * This property uniquely represents the transaction done by user.It is used to identify the transaction both by
 *  CA Strong Authentication Server and SDK.
 */
@property(retain, nonatomic) NSString * transactionId;

/**
 * This Property represents the transaction message which has the details of the transaction done by the User and this
 * will be displayed by the App to the User.
 */
@property(retain, nonatomic) NSString * transactionMessage;

/**
 * This property represents Display Name provided to the account provisioned on the User's device
 * by the user.
 */
@property(retain, nonatomic) NSString * displayName;

/**
 * This property represents the Authentication Types that are configured on the User's Device
 * eg: User PIN, BIOMETRIC
 */
@property(retain, nonatomic) NSMutableArray * authenticationTypes;

/**
 * This property provides the errorCode that gets returned depending on the scenario wherein
 * the TransactionDetailsResponse object gets populated
 */
@property(retain, nonatomic) NSString * errorCode;

/**
 *  This property provides the auto-enroll log id which is used to provision a new user during transaction
 *
 */
@property(retain, nonatomic) NSString * autoEnrollId;


/**
 *  Method                  - initWithData
 *  Description             - This API initializes the <CODE>TransactionDetailsResponse</CODE> object
 *                            with the Status,DisplayName,transactionId, TransactionMessage, AuthenticationTypes
 *  Args                    - NSString aStatus,NSString aTransactionId, NSString TransactionMessage,NSString displayName,AutheticationTypes authenticationTypes, NSString autoEnrollId
 *
 *  Return Type             - This API returns the type TransactionDetailsResponse object
 *                            TransactionDetailsResponse  -
 *                              status : SUCCESS/FAILURE
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
- (id)initWithData :(NSString *)aStatus TransactionId: (NSString *) aTransactionId TransactionMessage: (NSString *) aTransactionMessage DisplayName:(NSString *)aDisplayName AuthenticationTypes: (NSMutableArray *) aAuthenticationTypes AutoEnrollId:(NSString *)autoEnrollId;

/**
 *  Method                  - initWithData
 *  Description             - This API initializes the <CODE>TransactionDetailsResponse</CODE> object
 *                            with the Status,DisplayName,transactionId, TransactionMessage, AuthenticationTypes
 *  Args                    - NSString aStatus,NSString aTransactionId, NSString TransactionMessage,NSString displayName,AutheticationTypes authenticationTypes
 *
 *  Return Type             - This API returns the type TransactionDetailsResponse object
 *                            TransactionDetailsResponse  -
 *                              status : SUCCESS/FAILURE
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
- (id)initWithData :(NSString *)aStatus TransactionId: (NSString *) aTransactionId TransactionMessage: (NSString *) aTransactionMessage DisplayName:(NSString *)aDisplayName AuthenticationTypes: (NSMutableArray *) aAuthenticationTypes;

/**
 *  Method                  - initWithStatus
 *  Description             - This API initializes the <CODE>TransactionDetailsResponse</CODE> object
 *                            with the Status
 *  Args                    - NSString aStatus
 *  Return Type             - This API returns the type TransactionDetailsResponse object
 *                            TransactionDetailsResponse Parameters -
 *
 *                                  status : SUCCESS/FAILURE
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
-(id)initWithStatus:(NSString *)aStatus;

/**
 *  Method                  - initWithErrorCode
 *  Description             - This API initializes the <CODE>TransactionDetailsResponse</CODE> object with the errorCode
 *  Args                    - NSString errorCode
 *  Return Type             - This API returns the type TransactionDetailsResponse object
 *                            TransactionDetailsResponse
 *
 *
 *                            •    FAILURE: Operation failed.
 *                            If the status is FAILURE, then response object will have one of the below error code values.
 *                              -    ERROR_NO_ACTIVE_ACCOUNTS: No active accounts.
 *                              -    ERROR_SERVER_UNREACHABLE: Server is unreachable.
 *                              -    ERROR_ACCOUNT_NOT_FOUND: Invalid account.
 *                              -    ERROR_SERVER_INTERNAL: Server has returned an error.
 *                              -    ERROR_NO_TRANSACTION_AVAILABLE: There are no pending transactions.
 *                              -    INTERNAL_SERVER_ERROR: Internal error occurred.
 */
-(id)initWithErrorCode :(NSString *)errorCode;

@end

//
//  AutoEnrollmentResponse.h
//  M3DS
//
//  Created by husna02 on 23/05/19.
//  Copyright © 2019 Deepak Prasad. All rights reserved.
//

#ifndef AutoEnrollmentResponse_h
#define AutoEnrollmentResponse_h
#import "Header.h"

#endif /* AutoEnrollmentResponse_h */

/**
 * The AutoEnrollmentResponse class contains information about the results of the AutoEnrollment operation.
 * An object of this class is sent in the response of the autoEnroll method.
 */
@interface AutoEnrollmentResponse : NSObject


/**
 * This property represents the transactionId
 */
@property(retain, nonatomic) NSString * transactionId;

/**
 * This property determines whether the AutoEnrollment operation is SUCCESS/FAILURE
 */
@property(retain, nonatomic) NSString * status;

/**
 * This property provides the errorCode that gets returned depending on the scenario wherein
 * the AutoEnrollmentResponse object gets populated.
 */
@property(retain, nonatomic) NSString * errorCode;





/**
 *  Method                  - initWithData
 *  Description             - This API initializes the <CODE>AutoEnrollmentResponse</CODE> object
 *                            with the status,errorCode
 *  Args                    - NSString aStatus,NSString errorCode,
 *  Return Type             - This API returns the type AutoEnrollmentResponse object
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
-(id)initWithData :(NSString *)aTransactionId
                  :(NSString *)aStatus
                  :(NSString *)aErrorCode;

-(id)initWithStatus  :(NSString *)aStatus;


-(id)initWithErrorCode :(NSString *)_errorCode;




@end


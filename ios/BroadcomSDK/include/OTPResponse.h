//
//  OTPResponse.h
//  M3DS
//
//  Created by Babu Singh  on 10/31/17.
//  Copyright © 2017 Deepak Prasad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Header.h"

/**
 * The OTPResponse class contains information about an OTP.
 * An object of this class is sent in the response of the generateOTP method.
 */
@interface OTPResponse : NSObject

/**
 * This is the OTP that gets generated and returned to the Server for Validation for a given
 * Provisioned Account to the user when user
 * enters the User PIN
 */
@property(retain, nonatomic) NSString *otp;

/**
 * This property determines whether the status is SUCCESS/FAILURE
 */
@property(retain, nonatomic) NSString *status;

/**
 * This property provides the errorCode that gets returned depending on the scenario wherein
 * the OTPResponse object gets populated
 */
@property(retain, nonatomic) NSString *errorCode;

/**
 *  Method                  - initWithData
 *  Description             - This API initializes the <CODE>OTPResponse</CODE> object with the OTP,Status
 *  Args                    - NSString otp, NSString status
 *  Return Type             - This API returns the type OTPResponse object
 *                            OTPResponse  - status : SUCCESS/FAILURE
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
- (id) initWithData : (NSString *)mOtp status:(NSString *)mStatus;

/**
 *  Method                  - initWithStatus
 *  Description             - This API initializes the <CODE>OTPResponse</CODE> object with the Status
 *  Args                    - NSString status
 *  Return Type             - This API returns the type OTPResponse object
 *                            OTPResponse  - status       - SUCCESS
 *
 *
 *                            The status can be SUCCESS
 *                            •    SUCCESS: Operation was successful.
 *
 **/
- (id) initWithStatus : (NSString *)mStatus;

/**
 *  Method                  - initWithErrorCode
 *  Description             - This API initializes the <CODE>OTPResponse</CODE> object with the errorCode
 *  Args                    - NSString status
 *  Return Type             - This API returns the type OTPResponse object
 *
 *
 *                            The status can be only be FAILURE:
 *                            •    FAILURE: Operation failed.
 *
 *                            If the status is FAILURE, then
 *                                   one of the below error codes will be returned in the response object.
 *                                   •     ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                   •     ERROR_INVALID_PIN: Invalid PIN passed to the method.
 *
 **/
- (id) initWithErrorCode : (NSString *)_errorCode;

@end

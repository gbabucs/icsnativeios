//
//  CancelTransactionResponse.h
//  M3DS
//
//  Created by Babu Singh  on 3/12/18.
//  Copyright © 2018 Deepak Prasad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Header.h"

/**
 * The CancelTransactionResponse class contains information about the results of a Cancel Transaction operation.
 *  An object of this class is sent in the response of the cancelTransaction method.
 */
@interface CancelTransactionResponse : NSObject

/**
 * This property uniquely represents the transaction done by user.It is used to identify the transaction both by
 *  CA Strong Authentication Server and SDK.
 */
@property(retain, nonatomic) NSString * transactionId;


/**
 * This property determines whether the status is SUCCESS/FAILURE in the
 * CancelTransactionResponse object
 */
@property(retain, nonatomic) NSString * status;

/**
 * This property provides the errorCode that gets returned depending on the scenario wherein
 * the CancelTransactionResponse object gets populated.
 */
@property(retain, nonatomic) NSString * errorCode;


/**
 *  Method                  - initWithData
 *  Description             - This API initializes the <CODE>CancelTransactionResponse</CODE> object
 *                            with the Status,DisplayName
 *  Args                    - NSString aTransactionId, NSString aStatus
 *  Return Type             - This API returns the type CancelTransactionResponse object
 *                            CancelTransactionResponse  -
 *                              status : SUCCESS/FAILURE
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
- (id)initWithData: (NSString *)aTransactionId Status:(NSString *)aStatus;

/**
 *  Method                  - initWithStatus
 *  Description             - This API initializes the <CODE>CancelTransactionResponse</CODE> object
 *                            with the Status
 *  Args                    - NSString aStatus
 *  Return Type             - This API returns the type CancelTransactionResponse object
 *                            CancelTransactionResponse Parameters -
 *
 *                                  status : SUCCESS/FAILURE
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
-(id)initWithStatus:(NSString *)aStatus;


/**
 *  Method                  - initWithErrorCode
 *  Description             - This API initializes the <CODE>CancelTransactionResponse</CODE> object with the errorCode
 *  Args                    - NSString status
 *  Return Type             - This API returns the type CancelTransactionResponse object
 *                            AuthenticationResponse
 *
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If the status is FAILURE, then one of the below error codes will be returned in the response.
 *                                   -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                   -    ERROR_SERVER_UNREACHABLE: Server is unreachable.
 *
 */
-(id)initWithErrorCode :(NSString *)_errorCode;

@end

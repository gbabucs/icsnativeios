//
//  ActivationDetails.h
//  ecomm
//
//  Created by Deepak Prasad on 3/19/15.
//  Copyright (c) 2015 CA Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Header.h"

/**
 * The AccountActivationDetails class contains account activation details.
 * An object of this class is sent in the response of the verifyActivation method.
 */
@interface AccountActivationDetails : NSObject

/**
 * This property represents the account provisioned on the User's device.
 */
@property(retain, nonatomic) NSString * accountId;

/**
 * This property represents Display Name provided to the account provisioned on the User's device
 * by the user.
 */
@property(retain, nonatomic) NSString * displayName;

/**
 * This property determines whether the status is SUCCESS/FAILURE in the
 * AccountActivationDetails object
 */
@property(retain, nonatomic) NSString * status;

/**
 * This property provides the errorCode that gets returned depending on the scenario wherein
 * the AccountActivationDetails object gets populated
 */
@property(retain, nonatomic) NSString * errorCode;

/**
 * This Property checks whether is User PIN Required during activation of the account.
 */
@property Boolean isPINRequired;


/**
 *  Method                  - initWithData
 *  Description             - This API initializes the <CODE>AccountActivationDetails</CODE> object
 *                            with the Status,DisplayName
 *  Args                    - NSString aStatus, NSString aDisplayName
 *  Return Type             - This API returns the type AccountActivationDetails object
 *                            AccountActivationDetails  Parameters -
 *                                  status : SUCCESS/FAILURE,
 *                             displayName : User Provided Display Name.
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
- (id)initWithData :(NSString *)mAccountId Status: (NSString *)aStatus DisplayName:(NSString *)aDisplayName;

/**
 *  Method                  - initWithStatus
 *  Description             - This API initializes the <CODE>AccountActivationDetails</CODE> object
 *                            with the Status
 *  Args                    - NSString aStatus
 *  Return Type             - This API returns the type AccountActivationDetails object
 *                            AccountActivationDetails Parameters -
 *
 *                                  status : SUCCESS/FAILURE
 *                             displayName : User Provided Display Name.
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
-(id)initWithStatus:(NSString *)aStatus;

/**
 *  Method                  - initWithStatusAndDisplayName
 *  Description             - This API initializes the <CODE>AccountActivationDetails</CODE> object
 *                            with the Status, DisplayName
 *  Args                    - NSString aStatus, NSString aDisplayName
 *  Return Type             - This API returns the type AccountActivationDetails object
 *
 *                            AccountActivationDetails Parameters -
 *
 *                                  status : SUCCESS/FAILURE
 *                             displayName : User Provided Display Name.
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
-(id)initWithStatusAndDisplayName: (NSString *)aStatus DisplayName: (NSString *)aDisplayName;


/**
 *  Method                  - initWithErrorCode
 *  Description             - This API initializes the <CODE>AccountActivationDetails</CODE> object
 *                            with the errorCode
 *  Args                    - NSString errorCode
 *  Return Type             - This API returns the type AccountActivationDetails object
 *                            AccountActivationDetails  Parameters -
 *                              errorCode : SUCCESS/FAILURE
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 *                            If the status is FAILURE, the one of the below error codes value
 *                                   will be returned in the response object.
 *                                   -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                   -    ERROR_SERVER_UNREACHABLE: Server is unreachable.
 *                                   -    ERROR_DISPLAY_NAME_ALREADY_EXISTS: Display name already exists in the database.
 *                                   -    invalid_activation_code: User gave the invalid activation code.
 *                                   -    invalid_activation_ref_id: User gave the invalid reference id.
 *                                   -    INTERNAL_SERVER_ERROR: Internal server error occurred.
 *
 **/
-(id)initWithErrorCode :(NSString *)errorCode;

@end

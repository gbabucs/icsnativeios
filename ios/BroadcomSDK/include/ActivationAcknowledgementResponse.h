//
//  ActivationAckDetails.h
//  ecomm
//
//  Created by Deepak Prasad on 3/19/15.
//  Copyright (c) 2015 CA Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Header.h"

/**
 * The ActivationAcknowledgementResponse class contains activation acknowledgement details.
 * An object of this class is sent in the response of the setPin method.
 */
@interface ActivationAcknowledgementResponse : NSObject

/**
 * This property determines whether the status is SUCCESS/FAILURE in the
 * ActivationAcknowledgementResponse object
 */
@property(retain, nonatomic) NSString * status;

/**
 * This property provides the displayName entered by the User for a given Account.
 */
@property(retain, nonatomic) NSString * displayName;

/**
 * This property provides the errorCode that gets returned depending on the scenario wherein
 * the ActivationAcknowledgementResponse object gets populated
 */
@property(retain, nonatomic) NSString * errorCode;

/**
 *
 */
@property(retain, nonatomic) NSString * accountId;


/**
 *  Method                  - initWithData
 *  Description             - This API initializes the <CODE>ActivationAcknowledgementResponse</CODE> object
 *                            with the Status,DisplayName
 *  Args                    - NSString aStatus, NSString aDisplayName
 *  Return Type             - This API returns the type ActivationAcknowledgementResponse object
 *                            ActivationAcknowledgementResponse  - status : SUCCESS/FAILURE
 *
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
- (id)initWithData:(NSString *)aStatus DisplayName:(NSString *)aDisplayName;


/**
 *  Method                  - initWithStatus
 *  Description             - This API initializes the <CODE>ActivationAcknowledgementResponse</CODE> object
 *                            with the Status
 *  Args                    - NSString aStatus
 *  Return Type             - This API returns the type ActivationAcknowledgementResponse object
 *                            ActivationAcknowledgementResponse  - status : SUCCESS
 *
 *
 *                            The status can be only be SUCCESS:
 *                            •    SUCCESS: Operation was successful.
 *
 **/
- (id)initWithStatus:(NSString *)aStatus;

- (id)initWithParams:(NSString *)accountId:(NSString *)status:(NSString *)displayName:(NSString *)errorCode;


/**
 *  Method                  - initWithErrorCode
 *  Description             - This API initializes the <CODE>ActivationAcknowledgementResponse</CODE> object
 *                            with the errorCode
 *  Args                    - NSString errorCode
 *  Return Type             - This API returns the type ActivationAcknowledgementResponse object
 *                            ActivationAcknowledgementResponse  - errorCode : FAILURE
 *
 *
 *                            The status can be only be FAILURE
 *                            •    FAILURE: Operation failed.
 *
 *                            One of the below error codes value will be returned in the response object.
 *                                   •     ERROR_ACCOUNT_NOT_FOUND: Invalid account.
 *                                   •     ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                   •     ERROR_INVALID_PIN: Invalid PIN entered by the user.
 *
 **/
- (id)initWithErrorCode :(NSString *)errorCode;

-(NSMutableArray <ActivationAcknowledgementResponse *>*) populateFailureAckResponse:(NSMutableDictionary *)acctIdToMPNAccountDict;
-(NSMutableArray <ActivationAcknowledgementResponse *>*) populateLinkedActivationAckResponse:(NSMutableDictionary *)acknowledgementResponseDict:(NSMutableDictionary *)acctIdToM3DSAccountDict;

@end

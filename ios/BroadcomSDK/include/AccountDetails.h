//
//  CardDetails.h
//  M3DS
//
//  Created by Babu Singh  on 11/14/16.
//  Copyright © 2016 Deepak Prasad. All rights reserved.
//
#import<Foundation/Foundation.h>
#import "AuthenticationType.h"

#ifndef CardDetails_h
#define CardDetails_h


#endif /* CardDetails_h */

/**
 *  The AccountDetails contains information about an account.
 *  An object of this class is sent in the response of the getAllEnrolledAccounts method.
 */
@interface AccountDetails : NSObject

/**
 * This property represents the account provisioned on the User's device.
 */
@property (nonatomic, strong) NSString * accountId;

/**
 * This property represents the accountType - eg: Net-Banking, Credit/Debit Card depending on the Channel
 * and is provided by the CA Strong Authentication Server and saved in the SDK
 * when account  provisioned on the User's device.
 */
@property (nonatomic, strong) NSString * accountType;

/**
 * This property represents Display Name provided to the account provisioned on the User's device
 * by the user.
 */
@property (nonatomic, strong) NSString * displayName;


/**
 *  Method                  - initWithData
 *  Description             - This API initializes the <CODE>AccountDetails</CODE> object
 *                            with the Status,DisplayName
 *  Args                    - NSString mAccountId,NSString AccountType,NSString aDisplayName
 *  Return Type             - This API returns the type AccountDetails object
 *                            AccountDetails  Parameters -
 *                              mAccountId   : Account ID provided by the CA Strong Auth Service
 *                              status       : SUCCESS/FAILURE
 *                              AccountType  : Account Type provided by the CA Strong Auth Service
 *                              aDisplayName : Display Name provided by the User
 *
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
- (id) initWithData: (NSString *)mAccountId AccountType: (NSString *) mAccountType DisplayName:(NSString *)mDisplayName;

@end

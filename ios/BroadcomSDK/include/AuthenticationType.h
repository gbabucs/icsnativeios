//
//  AuthenticationType.h
//  M3DS
//
//  Created by Babu Singh  on 3/6/18.
//  Copyright © 2018 Deepak Prasad. All rights reserved.
//

#ifndef AuthenticationType_h
#define AuthenticationType_h

#endif /* AuthenticationType_h */

/**
 * The AuthenticationTypeStatus enum defines the statuses of an authentication type.
 */
typedef enum {
    DEVICE_PIN_AUTH = 0,
    USER_PIN_AUTH = 1,
    BIOMETRIC_AUTH = 2
}AuthenticationType;


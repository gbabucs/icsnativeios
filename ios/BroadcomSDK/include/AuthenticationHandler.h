//
//  M3DSHandler.h
//  ecomm
//
//  Created by Deepak Prasad on 3/19/15.
//  Copyright (c) 2015 CA Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransactionDetailsResponse.h"
#import "ActivationAcknowledgementResponse.h"
#import "AccountActivationDetails.h"
#import "AuthenticationResponse.h"
#import "AccountDeletionResponse.h"
#import "AccountDetails.h"
#import "OTPResponse.h"
#import "CancelTransactionResponse.h"
#import "Authentication.h"
#import "VerifyPINResponse.h"
#import "ChangePINResponse.h"
#import "UpdateAuthenticationResponse.h"
#import "ProfileInfo.h"
#import "MultipleAccountActivationParams.h"
#import "MultipleAccountActivationDetails.h"
#import "AutoEnrollmentResponse.h"

/**
 * This AuthenticationHandler class provides methods that can be called
 * by the business app or authenticator app. The app can use these methods
 * to activate CA Strong authentication for a user’s accounts
 * and to authenticate the user.
 */
@interface AuthenticationHandler : NSObject

#pragma Trasnsaction Methods

/**
 * Method                   - getTransaction
 * Description              - Gets the pending transactions for the accounts.
 *                            Returns an TransactionDetailsResponse object with below status/es.
 *                            •    SUCCESS: Operation was successful. This means one of the accounts has the pending transactions.
 *                            •    FAILURE: Operation failed.
 *                            If the status is FAILURE, then response object will have one of the below error code values.
 *                              -    ERROR_NO_ACTIVE_ACCOUNTS: No active accounts.
 *                              -    ERROR_SERVER_UNREACHABLE: Server is unreachable.
 *                              -    ERROR_ACCOUNT_NOT_FOUND: Invalid account.
 *                              -    ERROR_SERVER_INTERNAL: Server has returned an error.
 *                              -    ERROR_NO_TRANSACTION_AVAILABLE: There are no pending transactions.
 *                              -    INTERNAL_SERVER_ERROR: Internal error occurred.
 * Arguments                - None
 * Return Type              - <CODE>TransactionDetailsResponse</CODE>
 **/
- (TransactionDetailsResponse *) getTransaction;


/**
 * Method                   - getTransaction
 * Description              - Gets the pending transactions for a given <CODE>AccountId</CODE> of a given
 *                            <CODE>AccountType</CODE>
 *                            Returns an TransactionDetailsResponse object with below status/es.
 *                            •    SUCCESS: Operation was successful. This means one of the accounts has the pending transactions.
 *                            •    FAILURE: Operation failed.
 *                            If the status is FAILURE, then response object will have one of the below error code values.
 *                              -    ERROR_NO_ACTIVE_ACCOUNTS: No active accounts.
 *                              -    ERROR_SERVER_UNREACHABLE: Server is unreachable.
 *                              -    ERROR_ACCOUNT_NOT_FOUND: Invalid account.
 *                              -    ERROR_SERVER_INTERNAL: Server has returned an error.
 *                              -    ERROR_NO_TRANSACTION_AVAILABLE: There are no pending transactions.
 *                              -    INTERNAL_SERVER_ERROR: Internal error occurred.
 * Arguments                - None
 * Return Type              - <CODE>TransactionDetailsResponse</CODE>
 **/
-(TransactionDetailsResponse *) getTransaction: (NSString *) transactionId AccountId: (NSString *)accountId AccountType: (NSString *)accountType;

#pragma Authentication Method
/**
 * Method                   - authenticateUsingConsent
 * Description              - Authenticates the user after the user selects the Yes option displayed by the app.
 *                            No authentication prompt is displayed.
 *                            Returns an AuthenticationResponse object which contains the status value and error code value.
 *                            The status can be one of the following values:
 *                            •   SUCCESS: Operation was successful.
 *                            •   FAILURE: Operation failed.
 *                                  If the status is FAILURE, then one of the below error codes will be returned in the response.
 *                                  -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                  -    ERROR_SERVER_UNREACHABLE: Server is unreachable.
 *                                  -    ERROR_ACCOUNT_NOT_FOUND: Invalid account.
 *                                  -    ERROR_SERVER_INTERNAL: Server has returned an error.
 *                                  -    ERROR_INVALID_AUTHENTICATION_TYPE: Invalid AuthenticationType object present in the request.
 *
 * Arguments                - None
 * Return Type              - <CODE>AuthenticationResponse</CODE>
 **/
-(AuthenticationResponse *) authenticateUsingConsent:(TransactionDetailsResponse *) transactionDetailsResponse;

#pragma Verification Method
/**
 *   Method                   - verifyUsingTouchID
 *   Description              - Verification of  the user after the user selects the Touch ID.
 *                            verifyUsingTouchID.
 *                            verifyUsingTouchID object which contains the status value and error code value.
 *                            The status can be one of the following values:
 *                            •   SUCCESS: Operation was successful.
 *                            •   FAILURE: Operation failed.
 *                                  If the status is FAILURE, then one of the below error codes will be returned in the response.
 *                                  -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                  -    ERROR_SERVER_UNREACHABLE: Server is unreachable.
 *                                  -    ERROR_ACCOUNT_NOT_FOUND: Invalid account.
 *                                  -    ERROR_SERVER_INTERNAL: Server has returned an error.
 *                                  -    ERROR_INVALID_AUTHENTICATION_TYPE: Invalid AuthenticationType object present in the request.
 *
 * Arguments                - None
 * Return Type              - <CODE>AuthenticationResponse</CODE>
 * Please write the documentation for this kumar utsav
 **/

- (void) verifyPINUsingTouchID:(NSString *)touchIDForAutoEnroll
   withTxnalDetailsResponse:(TransactionDetailsResponse *)detailResponse
          SuccessHandler   :(void(^)(VerifyPINResponse *)) onSuccess
          FailureHandler   :(void(^)(VerifyPINResponse *)) onFailure
          FallbackHandler  :(void(^)(void)) onFallback
          CancelHandler    :(void(^)(void)) onCancel;
          
- (void) authenticateUsingTouchID:(TransactionDetailsResponse *) transactionDetailsResponse
                SuccessHandler   :(void(^)(AuthenticationResponse *)) onSuccess
                FailureHandler   :(void(^)(AuthenticationResponse *)) onFailure
                FallbackHandler  :(void(^)(void)) onFallback
                CancelHandler    :(void(^)(void)) onCancel;


/**
 * Method                    - authenticateUsingUserPin
 * Description               - Authenticates the user by using the user PIN.
 *                            The PIN prompt is displayed after the user selects the Yes option from the Yes/No options
 *                            displayed by the app.
 *                            Returns an AuthenticationResponse object.
 *                            The status can be one of the following values:
 *                            •   SUCCESS: Operation was successful.
 *                            •   FAILURE: Operation failed.
 *                                  If the status is FAILURE, then one of the below error codes will be returned in the response.
 *                                  -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                  -    ERROR_SERVER_UNREACHABLE: Server is unreachable.
 *                                  -    ERROR_ACCOUNT_NOT_FOUND: Invalid account.
 *                                  -    ERROR_SERVER_INTERNAL: Server has returned an error.
 *                                  -    ERROR_INVALID_AUTHENTICATION_TYPE: Invalid AuthenticationType object present in the request.
 * Arguments                - None
 * Return Type              - <CODE>AuthenticationResponse</CODE>
 **/
- (AuthenticationResponse *) authenticateUsingUserPin:(TransactionDetailsResponse *) transactionDetailsResponse :(NSString *)userPin;

/**
 * Method                   - cancelTransaction
 * Description              - Cancels the transaction specified by a TransactionDetailsResponse object.
 *                            Returns a CancelTransactionResponse object with status and errorCode.
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If the status is FAILURE, then one of the below error codes will be returned in the response.
 *                                   -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                   -    ERROR_SERVER_UNREACHABLE: Server is unreachable.
 *
 * Arguments                - accountId, userPin
 * Return Type              - <CODE>CancelTransactionResponse</CODE>
 **/
- (CancelTransactionResponse *) cancelTransaction:(TransactionDetailsResponse *) transactionDetailsResponse;


/**
 * Method                   - verifyActivation
 * Description              - Activates CA Strong Authentication for the second or subsequent account.
 *                            Returns an AccountActivationDetails object.
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If the status is FAILURE,
 *                                   then one of the below error codes will be returned in the response object.
 *                                   -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                   -    ERROR_SERVER_UNREACHABLE: Server is unreachable.
 *                                   -    ERROR_DISPLAY_NAME_ALREADY_EXISTS: Display name already exists in the database.
 *                                   -    invalid_activation_code: User gave the invalid activation code.
 *                                   -    invalid_activation_ref_id: User gave the invalid reference id.
 *                                   -    INTERNAL_SERVER_ERROR: Internal server error occurred.
 * Arguments                - accountId, userPin
 * Return Type              - <CODE> AccountActivationDetails </CODE>
 **/
- (AccountActivationDetails *) verifyActivation :(NSString *)activationCode
                                                :(NSString *)activationRefId
                                                :(NSString *)postURL
                                                :(NSString *)displayName
                                                :(NSString *)pin;
/**
 * Method                   - setPin
 * Description              - Sets the user PIN.Returns an ActivationAcknowledgementResponse object.
 * Arguments                - accountId, userPin
 * Return Type              - Returns <CODE> ActivationAcknowledgementResponse </CODE> with status and errorCode
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If the status is FAILURE, the one of the below error codes value
 *                                   will be returned in the response object.
 *                                   -    ERROR_ACCOUNT_NOT_FOUND: Invalid account.
 *                                   -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                   -    ERROR_INVALID_PIN: Invalid PIN entered by the user.
 *
 **/
- (ActivationAcknowledgementResponse *) setPin:(NSString *)accountId :(NSString *)userPin;

/**
 * Method                   - deleteAccount
 * Description              - Deletes an account.This method accepts an AccountDetails object as input.
 *                            Returns an AccountDeletionResponse object.
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If the status is FAILURE, then the one of the below error codes will be returned in the response.
 *                                   -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                   -    ERROR_ACCOUNT_NOT_FOUND: Invalid account.
 *                                   -    INTERNAL_SERVER_ERROR: Internal server error occurred.
 * Arguments                - AccountDetails
 * Return Type              - <CODE> AccountDeletionResponse </CODE>
 **/
#pragma CardDetails
-(AccountDeletionResponse *)deleteAccount: (AccountDetails *)mAccountDetails;

/**
 * Method                   - getAllEnrolledAccounts
 * Description              - Gets all accounts for which CA Strong Authentication has been activated.
 *                            Returns an Array of all enrolled AccountDetails objects.
 * Arguments                - None
 * Return Type              - <CODE> NSMutableArray </CODE>
 **/
#pragma void
-(NSMutableArray *) getAllEnrolledAccounts;

/**
 * Method                   - setURL
 * Description              - Updates the Service URL for the specified BIN ID.
 * Arguments                - OrgName, newURL
 * Return Type              - <CODE> BOOL </CODE>
 **/
#pragma void
-(BOOL) setURL: (NSString *) orgName : (NSString *) newURL;

/**
 * Method                   - generateOTP
 * Description              - Generates an OTP for the specified account..
 * Arguments                - AccountDetails
 * Return Type              - On Success - <CODE> OTPResponse </CODE>
 *                          - On Failure - <CODE> OTPResponse </CODE>
 *                          - On Cancel/Fallback - NONE
 **/
-(void) generateOTP : (AccountDetails *)mAccountDetails
      SuccessHandler: (void(^)(OTPResponse *)) onSuccess
      FailureHandler: (void(^)(OTPResponse *)) onFailure
      FallbackHandler  :(void(^)(void)) onFallback
      CancelHandler    :(void(^)(void)) onCancel;

/**
 * Method                   - generateOTP
 * Description              - Generates an OTP for the specified account..
 * Arguments                - AccountDetails, propertyDict and callback handlers for success, failure, fallback and cancel status
 * Return Type              - On Success - <CODE> OTPResponse </CODE>
 *                          - On Failure - <CODE> OTPResponse </CODE>
 *                          - On Cancel/Fallback - NONE
 **/
-(void) generateOTP : (AccountDetails *)mAccountDetails
      PropertyDict: (NSDictionary *)propertyDict
      SuccessHandler: (void(^)(OTPResponse *)) onSuccess
      FailureHandler: (void(^)(OTPResponse *)) onFailure
      FallbackHandler  :(void(^)(void)) onFallback
      CancelHandler    :(void(^)(void)) onCancel;

/**
 * Method                   - generateOTP
 * Description              - Generates an OTP for the specified account and Returns OTPResponse Object
 *                            with the status and errorCode
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If the status is FAILURE, then
 *                                   one of the below error codes will be returned in the response object.
 *                                   -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                   -    ERROR_INVALID_PIN: Invalid PIN passed to the method.
 *
 * Arguments                - AccountDetails, userPin
 * Return Type              - <CODE> OTPResponse </CODE>
 **/
-(OTPResponse *) generateOTP : (AccountDetails *)mAccountDetails userPin: (NSString *)userPin;

/**
 * Method                   - generateOTP
 * Description              - Generates an signed OTP for the specified account and Returns OTPResponse Object
 *                            with the status and errorCode
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If the status is FAILURE, then
 *                                   one of the below error codes will be returned in the response object.
 *                                   -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                   -    ERROR_INVALID_PIN: Invalid PIN passed to the method.
 *
 * Arguments                - AccountDetails, userPin, propertyDictionary
 * Return Type              - <CODE> OTPResponse </CODE>
 **/
-(OTPResponse *) generateOTP : (AccountDetails *)mAccountDetails userPin: (NSString *)userPin PropertyDict: (NSDictionary *)propertyDict;

/**
 *  Method                  - getAllAuthenticationTypes
 *  Description             - This API fetches the provided device modalities for a given Device
 *  Args                    - This API does not accept any arguments
 *  Return Type             - <CODE> NSMutableArray </CODE>
 **/
-(NSMutableArray *) getAllAuthenticationTypes;


/**
 *  Method                  - updateAuthenticationType
 *  Description             - This API updates the provided authenticationType in a given Device
 *  Args                    - (Authentication *)authentication
 *  Args                    - User PIN
 *  Return Type             - (BOOL) : TRUE [If update is successful]
 *                                   : FALSE [If update has failed]
 **/
- (UpdateAuthenticationResponse *) updateAuthenticationType :(Authentication *)authentication PIN :(NSString *)pin;

/**
 *   Method                 - isPINSetup
 *   Description            - This API checks whether PIN is setup or not in a given User Device
 *   Args                   - This API does not accept any arguments
 *   Return Type            - This API returns the type BOOL
 *                            value : TRUE [If PIN is setup]
 *                            value : FALSE [If PIN is not setup].
 **/
- (BOOL) isPINSetup;

/**
 *  Method                  - verifyPIN
 *  Description             - This API verifies the PIN provided and returns whether it is valid or not
 *  Args                    - NSString PIN
 *  Return Type             - This API returns the type VerifyPINResponse object
 *                            VerifyPINResponse  - status       - SUCCESS/FAILURE
 *                                               - errorMessage - Has the messages described depending
 *                                                                on above status
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If status is FAILURE,
 *                                   then one of the below error codes will be returned in the response object.
 *                                    -    ERROR_INVALID_PIN: Invalid user PIN.
 *                                    -    ERROR_SDK_INTERNAL: SDK error scenario.
 *                                    -    ERROR_NO_ACTIVE_ACCOUNTS: No active accounts are present.
 *                                    -    ERROR_SERVER_INTERNAL: Server has returned an error.
 *
 */

- (VerifyPINResponse *) verifyPIN :(NSString *)pin;

/**
 *  Method                  - verifyPIN
 *  Description             - This API verifies the PIN provided along with the signing key and returns whether it is valid or not
 *  Args                    - NSString PIN, propertyDict
 *  Return Type             - This API returns the type VerifyPINResponse object
 *                            VerifyPINResponse  - status       - SUCCESS/FAILURE
 *                                               - errorMessage - Has the messages described depending
 *                                                                on above status
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If status is FAILURE,
 *                                   then one of the below error codes will be returned in the response object.
 *                                    -    ERROR_INVALID_PIN: Invalid user PIN.
 *                                    -    ERROR_SDK_INTERNAL: SDK error scenario.
 *                                    -    ERROR_NO_ACTIVE_ACCOUNTS: No active accounts are present.
 *                                    -    ERROR_SERVER_INTERNAL: Server has returned an error.
 *
 */

- (VerifyPINResponse *) verifyPIN :(NSString *)pin PropertyDict: (NSDictionary *)propertyDict;


/**
 *  Method                  - verifyPIN
 *  Description             - This API verifies the PIN provided along with the signing key and returns whether it is valid or not
 *  Args                    - NSString PIN, autoEnrollId
 *  Return Type             - This API returns the type VerifyPINResponse object
 *                            VerifyPINResponse  - status       - SUCCESS/FAILURE
 *                                               - errorMessage - Has the messages described depending
 *                                                                on above status
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If status is FAILURE,
 *                                   then one of the below error codes will be returned in the response object.
 *                                    -    ERROR_INVALID_PIN: Invalid user PIN.
 *                                    -    ERROR_SDK_INTERNAL: SDK error scenario.
 *                                    -    ERROR_NO_ACTIVE_ACCOUNTS: No active accounts are present.
 *                                    -    ERROR_SERVER_INTERNAL: Server has returned an error.
 *
 */
- (VerifyPINResponse *) verifyPIN:(NSString *)pin :(NSString *)autoEnrollId;


/**
 *  Method                  - changePIN
 *  Description             - This API changes the PIN provided and returns whether it is valid or not
 *  Args                    - NSString oldPin, newPin
 *  Return Type             - This API returns the type ChangePINResponse object
 *                            ChangePINResponse  - status       - SUCCESS/FAILURE
 *
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If the status is FAILURE,
 *                                   then one of the below error codes will be returned in the response object.
 *                                   -    ERROR_INVALID_PIN: Invalid PIN passed to the method.
 *
 **/
- (ChangePINResponse *) changePIN :(NSString *)oldPin :(NSString *)newPin;

//API to get Enabled authentication types
-(NSMutableArray *)getEnabledAuthenticationTypes;


/**
 *  Method                  - getProfileInfo
 *  Description             - This API retrieves the User Profile details
 *                            configured in the Device ie., Device Nickname, Device Id
 *  Args                    - NONE
 *  Return Type             - ProfileInfo  - (NSString *)deviceNickName - The Name provided by the User during installation of the App
 *                                         - (NSString *)deviceId       - SAaaS generated User Device ID
 *                            In ProfileInfo, The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If status is FAILURE,
 *                                   then one of the below error codes will be returned in the response object.
 *                                   -   PROFILE_INFO_NOT_AVAILABLE: No profile available in the device.
 *
 */
-(ProfileInfo *)getProfileInfo;



/**
 * Method                   - verifyActivation
 * Description              - Activates CA Strong Authentication for the second or subsequent account.
 *                            Returns an AccountActivationDetails object.
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If the status is FAILURE,
 *                                   then one of the below error codes will be returned in the response object.
 *                                   -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                   -    ERROR_SERVER_UNREACHABLE: Server is unreachable.
 *                                   -    ERROR_DISPLAY_NAME_ALREADY_EXISTS: Display name already exists in the database.
 *                                   -    invalid_activation_code: User gave the invalid activation code.
 *                                   -    invalid_activation_ref_id: User gave the invalid reference id.
 *                                   -    INTERNAL_SERVER_ERROR: Internal server error occurred.
 * Arguments                - accountId, userPin
 * Return Type              - <CODE> AccountActivationDetails </CODE>
 **/
- (AccountActivationDetails *) verifyActivation :(NSString *)activationCode
                                                :(NSString *)activationRefId
                                                :(NSString *)postURL
                                                :(NSString *)displayName
                                                :(NSString *)pin;


/**
 * Method                   - verifyActivation
 * Description              - Activates CA Strong Authentication for the second or subsequent account.
 *                            Returns an AccountActivationDetails object.
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If the status is FAILURE,
 *                                   then one of the below error codes will be returned in the response object.
 *                                   -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                   -    ERROR_SERVER_UNREACHABLE: Server is unreachable.
 *                                   -    ERROR_DISPLAY_NAME_ALREADY_EXISTS: Display name already exists in the database.
 *                                   -    invalid_activation_code: User gave the invalid activation code.
 *                                   -    invalid_activation_ref_id: User gave the invalid reference id.
 *                                   -    INTERNAL_SERVER_ERROR: Internal server error occurred.
 * Arguments                - accountId, userPin
 * Return Type              - <CODE> AccountActivationDetails </CODE>
 **/
- (MultipleAccountActivationDetails *) verifyActivation :(NSMutableArray <MultipleAccountActivationParams *>*) multipleAccountActivationParamsArray
                                                            :(NSString *)serverURL
                                                            :(NSString *)pin;


  - (MultipleAccountActivationDetails *) verifyActivation :(NSMutableArray <MultipleAccountActivationParams *>*) multipleAccountActivationParamsArray
                                                              :(NSString *)serverURL;

-(NSMutableArray <ActivationAcknowledgementResponse *>*) setPin:(NSString*) userPin;

/**
 */
- (BOOL)areValidActivationParams:(NSMutableArray <MultipleAccountActivationParams *>*) multipleAccountActivationParamsArray;

- (BOOL)isValidActivationParam:(MultipleAccountActivationParams *)multipleAccountActivationParams;

-(BOOL)provisionMasterAccount:(NSString *)userCredential :(NSString *) masterAccountId :(NSString *)masterAccountProfile :(NSString *)masterAccountOrgName :(NSString *)serverUrl :(NSString *)userPin :(NSString *)masterAcctActivationCode;


-(BOOL)provisionAccount:(NSString *)accountId:(NSString *)accountType:(NSString *)deviceCredential:(NSString *)userCredential:(NSString *)deviceProfile:(NSString *)userProfile:(NSString *)displayName:(NSString *)orgName:(NSString *)serverUrl:(NSString *)activationCode:(NSString *)userPin;

-(BOOL)deleteProvisionedLinkedAccounts;


-(NSMutableDictionary *)createLinkedAccountAcknowledgementRequest;

- (BOOL) unenrollAccounts: (BOOL) removePinSetup removeFromRemoteAll : (BOOL) removeFromServer;

- (BOOL) unenrollAccounts: (NSArray<AccountDetails *> *) accounts removeFromRemote : (BOOL) removeFromServer;

- (VerifyPINResponse *) verifyPIN:(NSString *)pin :(TransactionDetailsResponse *)transactionDetailsResponse:(NSString *)autoEnrollId;

- (AutoEnrollmentResponse *)autoEnrollAccount:displayName;
-(NSString *) getSDKVersion;
@end

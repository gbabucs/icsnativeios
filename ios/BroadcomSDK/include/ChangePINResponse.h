//
//  ChangePINResponse.h
//  M3DS
//
//  Created by Babu Singh  on 7/6/18.
//  Copyright © 2018 Deepak Prasad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Header.h"

/**
 * The ChangePINResponse class contains information about the results of the Change PIN operation.
 * An object of this class is sent in the response of the changePIN method.
 */
@interface ChangePINResponse : NSObject


/**
 * This property determines whether the Change PIN operation is SUCCESS/FAILURE
 */
@property(retain, nonatomic) NSString * status;

/**
 * This property provides the errorCode that gets returned depending on the scenario wherein
 * the ChangePINResponse object gets populated.
 */
@property(retain, nonatomic) NSString * errorCode;


/**
 *  Method                  - initWithData
 *  Description             - This API initializes the <CODE>ChangePINResponse</CODE> object
 *                            with the status,errorCode
 *  Args                    - NSString aStatus,NSString errorCode,
 *  Return Type             - This API returns the type ChangePINResponse object
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
-(id)initWithData :(NSString *)aStatus
                  :(NSString *)errorCode;


/**
 *  Method                  - initWithStatus
 *  Description             - This API initializes the <CODE>ChangePINResponse</CODE> object
 *                            with the Status
 *  Args                    - NSString aStatus
 *  Return Type             - This API returns the type ChangePINResponse object
 *                            ChangePINResponse Parameters -
 *
 *                                  status : SUCCESS
 *
 *                            The status can be the following value:
 *                            •    SUCCESS: Operation was successful.
 *
 **/
-(id)initWithStatus:(NSString *)aStatus;


/**
 *  Method                  - initWithErrorCode
 *  Description             - This API initializes the <CODE>ChangePINResponse</CODE> object with the errorCode
 *  Args                    - NSString errorCode
 *  Return Type             - This API returns the type ChangePINResponse object
 *                            ChangePINResponse
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If the status is FAILURE,
 *                                   then one of the below error codes will be returned in the response object.
 *                                   -    ERROR_INVALID_PIN: Invalid PIN passed to the method.
 *
 * Return Type              - <CODE> ChangePINResponse </CODE>
 **/
-(id)initWithErrorCode: (NSString *)errorCode;

@end

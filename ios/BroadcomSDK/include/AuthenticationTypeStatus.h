//
//  AuthenticationTypeStatus.h
//  M3DS
//
//  Created by husna02 on 28/06/18.
//  Copyright © 2018 Deepak Prasad. All rights reserved.
//

#ifndef AuthenticationTypeStatus_h
#define AuthenticationTypeStatus_h


#endif /* AuthenticationTypeStatus_h */
/**
 * The AuthenticationTypeStatus enum defines the statuses of an authentication type.
 */

typedef enum {
    NA = -1,
    DISABLED = 0,
    ENABLED = 1,
} AuthenticationTypeStatus;

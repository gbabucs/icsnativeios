//
//  MultipleAccountActivationDetails.h
//  M3DS
//
//  Created by husna02 on 13/11/18.
//  Copyright © 2018 Deepak Prasad. All rights reserved.
//

#ifndef MultipleAccountActivationDetails_h
#define MultipleAccountActivationDetails_h


#endif /* MultipleAccountActivationDetails_h */
#import "MultipleAccountActivationStatus.h"
#import "ActivationAcknowledgementResponse.h"

/**
 * The MultipleAccountActivationDetails class contains account activation details.
 * An object of this class is sent in the response of the verifyActivation method.
 */
@interface MultipleAccountActivationDetails : NSObject





/**
 * This property determines whether the status is SUCCESS/FAILURE in the
 * MultipleAccountActivationDetails object
 */
@property(retain, nonatomic) NSString * status;

/**
 * This property provides the errorCode that gets returned depending on the scenario wherein
 * the MultipleAccountActivationDetails object gets populated
 */
@property(retain, nonatomic) NSString * errorCode;

@property Boolean isPINRequired;

/**
 * This property provides the activationResponses for multiple accounts
 */
@property NSMutableArray *multipleAccountActivationStatusArray;



/**
 *  Method                  - initWithData
 *  Description             - This API initializes the <CODE>MultipleAccountActivationDetails</CODE> object
 *                            with the Status,DisplayName
 *  Args                    - NSString aStatus, NSString aDisplayName
 *  Return Type             - This API returns the type MultipleAccountActivationDetails object
 *                            MultipleAccountActivationDetails  Parameters -
 *                                  status : SUCCESS/FAILURE,
 *                             displayName : User Provided Display Name.
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
- (id)initWithData:(NSMutableArray*)multipleAccountActivationStatusArray;

/**
 *  Method                  - initWithStatus
 *  Description             - This API initializes the <CODE>MultipleAccountActivationDetails</CODE> object
 *                            with the Status
 *  Args                    - NSString aStatus
 *  Return Type             - This API returns the type MultipleAccountActivationDetails object
 *                            MultipleAccountActivationDetails Parameters -
 *
 *                                  status : SUCCESS/FAILURE
 *                             displayName : User Provided Display Name.
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
-(id)initWithStatus:(NSString *)aStatus;



/**
 *  Method                  - initWithErrorCode
 *  Description             - This API initializes the <CODE>MultipleAccountActivationDetails</CODE> object
 *                            with the errorCode
 *  Args                    - NSString errorCode
 *  Return Type             - This API returns the type MultipleAccountActivationDetails object
 *                            MultipleAccountActivationDetails  Parameters -
 *                              errorCode : SUCCESS/FAILURE
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 *                            If the status is FAILURE, the one of the below error codes value
 *                                   will be returned in the response object.
 *                                   -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                   -    ERROR_SERVER_UNREACHABLE: Server is unreachable.
 *                                   -    ERROR_DISPLAY_NAME_ALREADY_EXISTS: Display name already exists in the database.
 *                                   -    invalid_activation_code: User gave the invalid activation code.
 *                                   -    invalid_activation_ref_id: User gave the invalid reference id.
 *                                   -    INTERNAL_SERVER_ERROR: Internal server error occurred.
 *
 **/
-(id)initWithErrorCode :(NSString *)errorCode;




-(MultipleAccountActivationDetails *) mergeMultipleActivationAcknowledgementResponses:(MultipleAccountActivationDetails *)multipleAccountActivationDetails
                                                                                     :(NSMutableArray<ActivationAcknowledgementResponse*>*)acknowledgementResponses;



@end

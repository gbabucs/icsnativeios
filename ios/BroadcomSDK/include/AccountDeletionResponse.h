//
//  CardDeletionResponse.h
//  M3DS
//
//  Created by Babu Singh  on 11/14/16.
//  Copyright © 2016 Deepak Prasad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Header.h"

#ifndef CardDeletionResponse_h
#define CardDeletionResponse_h

#endif
/* CardDeletionResponse_h */

/**
 * The AccountDeletionResponse class contains information about the results of an Account Deletion operation.
 * An object of this class is sent in the response of the deleteAccount method.
 */
@interface AccountDeletionResponse : NSObject

/**
 * This property represents whether the deletion operation is SUCCESS/FAILURE
 */
@property (nonatomic, strong) NSString * status;

/**
 * This property represents Display Name provided to the account provisioned on the User's device
 * by the user.
 */
@property (nonatomic, strong) NSString * displayName;


/**
 * This property provides the errorCode that gets returned depending on the scenario wherein
 * the AccountDeletionResponse object gets populated.
 */
@property (nonatomic, strong) NSString * errorCode;

/**
 *  Method                  - initWithData
 *  Description             - This API initializes the <CODE>AccountDeletionResponse</CODE> object
 *                            with the status,displayName
 *  Args                    - NSString aStatus,NSString displayName,
 *  Return Type             - This API returns the type AccountDeletionResponse object
 *                            AccountDeletionResponse  -
 *                              displayName : display name provided by User
 *                              status : SUCCESS/FAILURE
 *
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *
 **/
-(id)initWithData: (NSString *)mStatus DisplayName: (NSString *)mDisplayName;


/**
 *  Method                  - initWithStatus
 *  Description             - This API initializes the <CODE>AccountDeletionResponse</CODE> object
 *                            with the Status
 *  Args                    - NSString aStatus
 *  Return Type             - This API returns the type AccountDeletionResponse object
 *                            AccountDeletionResponse Parameters -
 *
 *                                  status : SUCCESS
 *
 *                            The status can be the following value:
 *                            •    SUCCESS: Operation was successful.
 *
 *
 **/
-(id)initWithStatus:(NSString *)aStatus;

/**
 *  Method                  - initWithErrorCode
 *  Description             - This API initializes the <CODE>AccountDeletionResponse</CODE> object with the errorCode
 *  Args                    - NSString errorCode
 *  Return Type             - This API returns the type AccountDeletionResponse object
 *                            AccountDeletionResponse
 *                            The status can be one of the following values:
 *                            •    SUCCESS: Operation was successful.
 *                            •    FAILURE: Operation failed.
 *                                   If the status is FAILURE, then the one of the below error codes will be returned in the response.
 *                                   -    ERROR_INVALID_ARGUMENT: Incorrect details were passed.
 *                                   -    ERROR_ACCOUNT_NOT_FOUND: Invalid account.
 *                                   -    INTERNAL_SERVER_ERROR: Internal server error occurred.
 *
 * Return Type              - <CODE> AccountDeletionResponse </CODE>
 **/
-(id)initWithErrorCode :(NSString *)errorCode;



@end

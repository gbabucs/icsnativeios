//
//  StrongAuth.m
//  CAReactNative
//
//  Created by ThunderFlash on 12/12/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "React/RCTBridgeModule.h"
#import "React/RCTEventEmitter.h"

@interface RCT_EXTERN_MODULE(StrongAuth, NSObject)
RCT_EXTERN_METHOD(verifySingleAccountActivation:(NSString *)activationCode activationRefId:(NSString *)activationRefId postURL:(NSString *)postURL displayName:(NSString *)displayName pin:(NSString *)pin resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
RCT_EXTERN_METHOD(verifyActivation:(NSArray *)multipleAccountActivationParams callback:(RCTResponseSenderBlock))
RCT_EXTERN_METHOD(getTransaction:(NSString *)transactionId accountId:(NSString *)accountId accountType:(NSString *)accountType displayName:(NSString *)displayName callback:(RCTResponseSenderBlock))
RCT_EXTERN_METHOD(setPin:(NSString *)accountId pin:(NSString *)pin resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
RCT_EXTERN_METHOD(changePIN:(NSString *)oldPin newPin:(NSString *)newPin callback:(RCTResponseSenderBlock))
RCT_EXTERN_METHOD(verifyPIN:(NSString *)pin autoEnrollId:(NSString *)autoEnrollId callback:(RCTResponseSenderBlock))
RCT_EXTERN_METHOD(isPINSetup:(RCTResponseSenderBlock *))
@end

